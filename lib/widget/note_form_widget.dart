import 'package:flutter/material.dart';

class NoteFormWidget extends StatefulWidget {
  final bool? isImportant;
  final int? number;
  final String? title;
  final String? description;
  final ValueChanged<int> onChangedNumber;
  final ValueChanged<String> onChangedTitle;
  final ValueChanged<String> onChangedDescription;

  const NoteFormWidget({
    Key? key,
    this.isImportant = false,
    this.number = 0,
    this.title = '',
    this.description = '',
    required this.onChangedNumber,
    required this.onChangedTitle,
    required this.onChangedDescription,
  }) : super(key: key);

  _NoteFormWidget createState() => _NoteFormWidget();
}

// ignore: camel_case_types
class _NoteFormWidget extends State<NoteFormWidget> {
  late int number = widget.number!.toInt();
  void _pluss() {
    setState(() {
      number++;
      widget.onChangedNumber(number);
    });
  }

  void _minus() {
    setState(() {
      number--;
      if (number < 0) {
        number = 0;
      }
      widget.onChangedNumber(number);
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildTitle(),
              SizedBox(height: 8),
              buildDescription(),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
      bottomNavigationBar: pesan());

  Widget buildTitle() => TextFormField(
        maxLines: 1,
        initialValue: widget.title,
        style: TextStyle(
          color: Colors.grey[800],
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Nama Pesanan',
          hintStyle: TextStyle(color: Colors.grey[700]),
        ),
        validator: (title) =>
            title != null && title.isEmpty ? 'Pesanan Tidak Ada' : null,
        onChanged: widget.onChangedTitle,
      );

  Widget buildDescription() => TextFormField(
        maxLines: 30,
        initialValue: widget.description,
        style: TextStyle(color: Colors.grey[900], fontSize: 18),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Deskripsi Pembelian',
          hintStyle: TextStyle(color: Colors.grey[900]),
        ),
        validator: (title) =>
            title != null && title.isEmpty ? 'Deskripsi tidak ada' : null,
        onChanged: widget.onChangedDescription,
      );

  Widget pesan() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Row(
            children: [
              Text(
                "Pesanan",
                style: TextStyle(fontSize: 20),
              ),
              IconButton(icon: Icon(Icons.minimize), onPressed: _minus),
              Text(
                '$number',
                style: TextStyle(fontSize: 20),
              ),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: _pluss,
                alignment: Alignment.center,
              )
            ],
          ),
        ),
      );
}
